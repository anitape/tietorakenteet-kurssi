package tar18_treeset;

public class BinaryTree {
	private Node root;
    public static BinaryTree found; // findWithPreOrder()-operaation apurakenne

    public BinaryTree(int rootValue) {
        root = new Node(rootValue);
    }
    
    public void addSolmu(int avain){
        if(root == null){
            root = new Node(avain);
        } else if(root.getData() > avain) { //Jos avain pienempi, laita vasemmalle
            if(root.left() == null){
                BinaryTree newTree = new BinaryTree(avain);
                root.setLeft(newTree);
            } else {
                root.left().addSolmu(avain);
            }
        } else {  // Jos avain suurempi, laita oikealle
            if(root.right() == null){
                BinaryTree newTree = new BinaryTree(avain);
                root.setRight(newTree);
            } else {
                root.right().addSolmu(avain);
            }
        }
    }
    
    public boolean haeSolmu(int avain){
        if(root == null){
            System.out.println("Root NULL");
        } else if(root.getData() == avain){ // Jos avain sama
            return true;
        } else if(root.getData() > avain){ // Jos avain pienempi, etsi vasemmalta
            if(root.left() == null){
                return false;
            } else {
                root.left().haeSolmu(avain);
            }
        } else {  // Jos avain suurempi, etsi oikealata
            if(root.right() == null){
                return false;
            } else {
                root.right().haeSolmu(avain);
            }
        }
        return false;
    }

    /*public BinaryTree(String rootValue, BinaryTree left, BinaryTree right){
        root = new Node(rootValue, left, right);
    } */

    public void preOrder() {
        if (root != null) {
            System.out.println(root.getData());
            if (root.left() != null) // pääseeekö vasemmalle?
                root.left().preOrder();
            if (root.right() != null) // pääseekö oikealle?
                root.right().preOrder();
        }

    }

    // löydetty alipuu asetetaan staattiseen muuttujaan found

    public void setNotFound() {
        found = null;
    }
    public static BinaryTree getFound() {
        return found;
    }

    public void setLeft(BinaryTree tree) {
        root.setLeft(tree);
    }

    public void setRight(BinaryTree tree) {
        root.setRight(tree);
    }

}
