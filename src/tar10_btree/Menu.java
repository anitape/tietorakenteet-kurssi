/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tar10_btree;

/**
 *
 * @author kamaj
 */
public class Menu {
//main alkaa-----------------------------------------------------------------------------
        public static void main(String[] args) {

                        printMenu();

        }
//main loppuu --------------------------------------------------------------------------
//printMenu alkaa------------------------------------------------------------------
        private static void printMenu() {
                char select, select1;
                BinaryTree tree = null, upDated = null;
                int data;
                do {

                        System.out.println("\n\t\t\t1. Luo juurisolmu.");
                        System.out.println("\t\t\t2. Käy puu läpi esijärjestyksessä.");
                        System.out.println("\t\t\t3. Lisää avaimella.");
                        System.out.println("\t\t\t4. Hae avaimella.");
                        System.out.println("\t\t\t5. Hae korkeus avaimella.");
                        System.out.println("\t\t\t6. Lopetus. ");
                        System.out.print("\n\n"); // tehdään tyhjiä rivejä
                        select = Lue.merkki();
                        switch (select) {
                        case '1':
                            System.out.println("Anna juuren sisältö");
                            data = Lue.kluku();
                            tree = new BinaryTree(data);
                            break;
                        case '2':
                            tree.preOrder();
                            char h = Lue.merkki(); // pysäytetään kontrolli
                            break;
                        case '3':
                        	System.out.println("Anna solmun sisältö");
                        	if (tree == null)
                                System.out.println("Et ole muodostanut juurisolmua.");
                            else {
                                tree.addSolmu(Lue.kluku());
                            }

                            break;
                        case '4':
                        	System.out.println("Anna haettava avain");
                            if (tree == null)
                                System.out.println("Et ole muodostanut juurisolmua.");
                            else {
                                tree.haeSolmu(Lue.kluku());
                            }
                            break;
                        case '5':
            				System.out.println("Anna haluamasi avain (kokonaisluku)");
            				int haku = Lue.kluku();
            				if (tree != null) {
            					tree.getHeight(haku);
            				} else {
            					System.out.println("Avainta ei löytynyt.");
            				}
            				break;
                        case '7':
                            break;
    
                        }                    
                }
                while (select != '7');
        }
//printMenu loppuu ----------------------------------------------------------------
}

