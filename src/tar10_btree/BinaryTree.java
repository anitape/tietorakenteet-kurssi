/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tar10_btree;

/**
 *
 * @author kamaj
 */
public class BinaryTree {

	private Node root;
	public static BinaryTree found; // findWithPreOrder()-operaation apurakenne
	int kierros = 0;

	public BinaryTree(int rootValue) {
		root = new Node(rootValue);
	}

	public void addSolmu(int avain) {
		if (root == null) {
			root = new Node(avain);
		} else if (root.getData() > avain) { // Jos avain pienempi, laita vasemmalle
			if (root.left() == null) {
				BinaryTree newTree = new BinaryTree(avain);
				root.setLeft(newTree);
				System.out.println("Lisätty vasemmalle - " + avain);
			} else {
				root.left().addSolmu(avain);
			}
		} else { // Jos avain suurempi, laita oikealle
			if (root.right() == null) {
				BinaryTree newTree = new BinaryTree(avain);
				root.setRight(newTree);
				System.out.println("Lisätty oikealle - " + avain);
			} else {
				root.right().addSolmu(avain);
			}
		}
	}

	public void haeSolmu(int avain) {
		if (root == null) {
			System.out.println("Root NULL");
		} else if (root.getData() == avain) { // Jos avain sama
			System.out.println("Löydetty avain " + avain);
		} else if (root.getData() > avain) { // Jos avain pienempi, etsi vasemmalta
			if (root.left() == null) {
				System.out.println("Ei löydetty vasemmalta - " + avain);
			} else {
				System.out.println("Mennään vasemmalle...");
				root.left().haeSolmu(avain);
			}
		} else { // Jos avain suurempi, etsi oikealata
			if (root.right() == null) {
				System.out.println("Ei löydetty oikealta - " + avain);
			} else {
				System.out.println("Mennään oikealle...");
				root.right().haeSolmu(avain);
			}
		}
	}

	// method to check if a node is leaf or not
	public boolean isLeaf(int avain) {
		if (root.right() == null && root.left() == null && root.getData() == avain) {
			return true;
			}
		return false;
	}

	// function to return maximum of two numbers
	public int getMax(int a, int b) {
		return (a > b) ? a : b;
	}

	// function to get the height of a tree or node
	public int getHeight(int avain) {
		if (root == null || isLeaf(avain)) { // height will be 0 if the node is leaf or null
			System.out.println("En löytänyt korkeutta");
			return 0;
		}
		// height of a node will be 1+ greater among height of right subtree and height
		// of left subtree
		System.out.println("Pitäisi laskea korkeus.");
		return (getMax(root.left().getHeight(avain), root.right().getHeight(avain) + 1));
	}

	/*
	 * public BinaryTree(String rootValue, BinaryTree left, BinaryTree right){ root
	 * = new Node(rootValue, left, right); }
	 */

	public void preOrder() {
		if (root != null) {
			System.out.println(root.getData());
			if (root.left() != null) // pääseeekö vasemmalle?
				root.left().preOrder();
			if (root.right() != null) // pääseekö oikealle?
				root.right().preOrder();
		}

	}

	// löydetty alipuu asetetaan staattiseen muuttujaan found
	public void findWithPreOrder() {

		if (root != null) {
			System.out.print(root.getData() + ": muokkaatko tätä?");
			if (root.left() == null)
				System.out.print(" (vasemmalla tilaa)");
			if (root.right() == null)
				System.out.println(" (oikealla tilaa)");
			char select = Lue.merkki();
			if (select == 'k') {
				found = this;
				return;
			}
			if (found == null && root.left() != null) // pääseekö vasemmalle?
				root.left().findWithPreOrder();
			if (found == null && root.right() != null) // pääseekö oikealle?
				root.right().findWithPreOrder();
		}

	}

	public void setNotFound() {
		found = null;
	}

	public static BinaryTree getFound() {
		return found;
	}

	public void setLeft(BinaryTree tree) {
		root.setLeft(tree);
	}

	public void setRight(BinaryTree tree) {
		root.setRight(tree);
	}
}
