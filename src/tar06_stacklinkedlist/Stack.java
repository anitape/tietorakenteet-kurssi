package tar06_stacklinkedlist;

import java.util.Iterator;
import java.util.LinkedList;

public class Stack {

	private LinkedList<String> list;

	public Stack() {
		list = new LinkedList<String>();
	}

	public void push(String aData) {
		list.add(aData);
	}

	public String pop() {
		Object oldTop = null;

		Iterator itr = list.iterator();

		while (itr.hasNext()) {
			oldTop = itr.next();
		}

		if (oldTop != null) {
			list.remove(oldTop);
			return oldTop.toString();
		} else {
			return null;
		}

	}

	public int getSize() {
		return list.size();
	}

	public void printItems() {
		Iterator itr = list.iterator();
		while (itr.hasNext()) {
			System.out.println(itr.next());
		}
	}

}