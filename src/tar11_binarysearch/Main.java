package tar11_binarysearch;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Main {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BinarySearch bins = new BinarySearch();
		int count = 15;
		int[] test = new int[count];
		for (int i = 0;i<count;i++) {
			int random = (int)(Math.random() * count)+1;
			test[i] = random;
			bins.add(random);
		}
		System.out.println("Syötetty taulukko");
		System.out.println(Arrays.toString(test).toString()+"\n");
		System.out.println("Taulukon minimiarvo on: " + bins.getMin(test));
		System.out.println("Taulukon maximiarvo on: " + bins.getMax(test));
		System.out.println();
		System.out.println("Järjestettu taulukko");
		System.out.println(bins.toString()+"\n");

		System.out.println("Testiarvot");      
		for (int j = 0;j<count/2;j++) {
			int random = (int)(Math.random() * count)+1;
			System.out.println("Finding "+random+"\tStatus : "+bins.find(random));			
		}
		
	}


}
