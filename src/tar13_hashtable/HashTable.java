package tar13_hashtable;

public class HashTable {
	 private int size = 10;
	    private int[] array;
	    private int full;
	    
	    public HashTable(){
	        array = new int[size];
	        this.size = size;
	        full = 0;
	    }
	    
	    public void addKey(int key){
	        int index = key%size; //hajautus
	        
	        if (key == 0){
	            // ei sallita nollaa
	        }
	        if (array[index] == 0){ // jos tämä taulukon paikka on tyhjä
	        	System.out.println("h(" + key + ") = " + index);
	            array[index] = key; // avain lisätään taulukkoon
	            full++;
	        } else {
	        	System.out.println("h(" + key + ") = " + index+ " !");
	            collision(index, key); //jos avain löytyy siitä paikasta kutsutaan yhteentörmays-metodi
	            System.out.println(" => ");
	        }	        
	    }
	    //yhteentörmäys
	    public void collision(int index, int data){
	        boolean onnistui = false;
	        int paikka; 	        
	        paikka=index+1; //uusi paikka
	        
	        while(!onnistui && full!=size){ // looppi pyörii niin kauan kunnes avain löytää paikan talukosta ja taukossa on vielä tyhjiä paikkoja
	            if (paikka>=size){
	                paikka=0; //taulukossa ei enää paikkoja
	                }
	            if (array[paikka]==0){ // jos tämä taulukon paikka on tyhjä
	            	System.out.println("h(" + data + ") = " + paikka);
	                array[paikka] = data; //talleta avain
	                full++;
	                onnistui = true;
	            } else {
	            	System.out.println("h(" + data + ") = " + paikka + " !");
	                paikka=paikka+1; //uusi paikka
	                System.out.println(" => " + paikka);
	            }
	        }
	    }
	    
	    public void searchKey(int key){
	        int index = key%size;
	        boolean found = false;
	        
	        if (array[index]!= key){
	            
	            while(!found){
	                index++;
	                if (index>=size){
	                System.out.println("Arvoa ei löytynyt!");
	                break;
	                }
	                if (array[index] == key){
	                    found=true;
	                } else {
	                    found=false;
	                }
	                
	            }
	            if (index==size){
	            } else{
	            System.out.println("Arvo löytyi indeksillä: " + index);
	            }
	        } else {
	            found=true;
	            System.out.println("Arvo löytyi indeksillä: " + index);
	        }
	        
	    }	    
	    
	    public void print(){
	        System.out.print("Taulukko: ");
	        for (int i=0; i<size; i++){
	            System.out.print(array[i] + ", ");
	        }
	        System.out.println();
	    }

}
