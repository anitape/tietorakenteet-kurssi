package tar01_stackproject;

public class Stack {
	private ListItem mTop; // viite pinon huipulle
    private int mSize;// pinottujen alkioiden lkm
    
    public Stack() {
    	mTop = null;
    }
    // luo uusi lista-alkio, vie se pinon huipulle
    public void push(String aData){
        ListItem newItem = new ListItem();
        newItem.setData(aData);
        newItem.setNext(mTop);
        mTop = newItem;
        mSize++;
    }
    
    // palauta pinon huipulla oleva alkio, jos pinossa ei ole
    // mitään palauta null
    public ListItem pop() {
        ListItem returned = null;
        if (mTop!= null) {
        	returned = mTop;
        	mTop = mTop.getNext();
        	mSize--;
        }
        return returned;
    }
    
    // palauta pinottujen alkioiden lkm
    public int getSize() {
        return mSize;
    }
    
    // tulosta pinon sisältö muuttamatta pinoa
    public void printItems() {
        ListItem scanner = mTop;
        while (scanner != null) {
        	System.out.print(scanner.getData()+" ");
        	scanner = scanner.getNext();
        }
    }
}
