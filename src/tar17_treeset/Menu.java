/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tar17_treeset;

import java.util.Iterator;
import java.util.TreeSet;


/**
 *
 * @author kamaj
 */
public class Menu {
//main alkaa-----------------------------------------------------------------------------
	public static void main(String[] args) {

		printMenu();

	}

//main loppuu --------------------------------------------------------------------------
//printMenu alkaa------------------------------------------------------------------
	private static void printMenu() {
		char select, select1;
		TreeSet<Integer> tree = new TreeSet<>(); //toimii lisäys, haku ja inorder, mutta preorder järjestys ei onnistu
		int data;
		int luku;
		Integer obj1;
		Integer obj;
		do {

			System.out.println("\n\t\t\t1. Lisää uusi solmu avaimella.");
			System.out.println("\t\t\t2. Hae avaimella.");
			System.out.println("\t\t\t3. Käy puu läpi sisäjärjestyksessä.");
			System.out.println("\t\t\t4. Lopetus. ");
			System.out.print("\n\n"); // tehdään tyhjiä rivejä
			select = Lue.merkki();
			switch (select) {
			case '1':
				System.out.println("Anna solmun sisältö");
				data = Lue.kluku();
				obj1 = new Integer(data);
				tree.add(obj1);
				break;
			case '2':
				System.out.println("Anna haettava avain");
				luku = Lue.kluku();
				obj = new Integer(luku);
				if(tree.contains(obj)) {
					System.out.println("Avain " + luku + " löytyi");
				}
				else {
					System.out.println("Avainta " + luku + " ei löytynyt");
				};
				break;
			case '3':
				Iterator<Integer> itr = tree.iterator();  
				  while(itr.hasNext()){  
				   System.out.println(itr.next());  
				  }  
				char h = Lue.merkki(); // pysäytetään kontrolli
				break;
			case '4':
				break;

			}
		} while (select != '4');
	}


//printMenu loppuu ----------------------------------------------------------------
}
