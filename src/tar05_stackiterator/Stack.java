/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tar05_stackiterator;

import java.util.EmptyStackException;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 *
 * @author kamaj
 */
public class Stack {

      	ListItem top; // top näkyy oletuspakkaukseen
      	int size;
        
      	String []items;
       

        public Stack(int m) {
        	items = new String[m];
            size = 0;
        }
        
        //  palautetaan pino-iteraattori
        public StackIterator iterator() {
            return new StackIterator(this);
        }
        
        // muodostetaan uusi alkio ja viedään se huipulle
        public void push(String aData) {   
        	if (size == items.length) {
        		throw new RuntimeException("no space");
        	}
        	items[size] = aData;
        	size = size+1; 
        }
        
               
        // poistetaan alkio pinon huipulta, jos pinossa ei alkioita palautetaan null
        public String pop() {
        	if (size == 0) throw new EmptyStackException();
        	size=size-1; 
        	return items[size];
        }
        
        // palautetaan pinottujen alkioiden lukumäärä
        public int getSize() {
                return size;
        }
   
        
        // listataan sisältö
        public void printItems() { 
        		ListItem lPointer = top;
                while (lPointer != null) {
                        System.out.print(lPointer.getData()+", ");
                        lPointer = lPointer.getLink();
                }

        }
}

