/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tar08_btree;

/**
 *
 * @author kamaj
 */
public class Menu {
//main alkaa-----------------------------------------------------------------------------
        public static void main(String[] args) {

                        printMenu();

        }
//main loppuu --------------------------------------------------------------------------
//printMenu alkaa------------------------------------------------------------------
        private static void printMenu() {
                char select, select1;
                BinaryTree tree = null, upDated = null;
                int data;
                do {

                        System.out.println("\n\t\t\t1. Luo juurisolmu.");
                        System.out.println("\t\t\t2. Päivitä uusi solmu.");
                        System.out.println("\t\t\t3. Käy puu läpi esijärjestyksessä.");
                        System.out.println("\t\t\t4. Lisää avaimella.");
                        System.out.println("\t\t\t5. Hae avaimella.");
                        System.out.println("\t\t\t6. Hae korkeus avaimella.");
                        System.out.println("\t\t\t7. Lopetus. ");
                        System.out.print("\n\n"); // tehdään tyhjiä rivejä
                        select = Lue.merkki();
                        switch (select) {
                        case '1':
                            System.out.println("Anna juuren sisältö");
                            data = Lue.kluku();
                            tree = new BinaryTree(data);
                            break;
                        case '2':
                            if (tree == null)
                                System.out.println("Et ole muodostanut juurisolmua.");
                            else {
                                System.out.println("Anna solmun sisältö");
                                BinaryTree newTree = new BinaryTree(Lue.kluku());

                                tree.setNotFound();
                                tree.findWithPreOrder();
                                upDated = BinaryTree.getFound();
                                if (upDated==null) // ei valittu mitään
                                    break;
                                System.out.print("Kytke vasemmalle? (k/e)");
                                select1 = Lue.merkki();
                                if (select1=='k')
                                    upDated.setLeft(newTree);
                                else {
                                    System.out.print("Kytke oikealle? (k/e)");
                                    select1 = Lue.merkki();
                                    if (select1=='k')
                                        upDated.setRight(newTree);
                                }
                            }
                            break;
                        case '3':
                            tree.preOrder();
                            char h = Lue.merkki(); // pysäytetään kontrolli
                            break;
                        case '4':
                        	System.out.println("Anna solmun sisältö");
                        	if (tree == null)
                                System.out.println("Et ole muodostanut juurisolmua.");
                            else {
                                tree.addSolmu(Lue.kluku());
                            }

                            break;
                        case '5':
                        	System.out.println("Anna haettava avain");
                            if (tree == null)
                                System.out.println("Et ole muodostanut juurisolmua.");
                            else {
                                tree.haeSolmu(Lue.kluku());
                            }
                            break;
                        case '6':
            				System.out.println("Anna haluamasi avain (kokonaisluku)");
            				int hakukierros = tree.haeSolmu1(Lue.kluku());
            				int yhte = tree.preOrder1();
            				int korkeus = yhte - hakukierros;
            				if (tree != null) {
            					//height = tree.height(haku);
            					System.out.println("Hakukierros  " + hakukierros);
            					System.out.println("Yhteensä  " + yhte);
            					System.out.println("Puun korkeus " + korkeus);
            				} else {
            					System.out.println("Avainta ei löytynyt.");
            				}
            				break;
                        case '7':
                            break;
    
                        }                    
                }
                while (select != '7');
        }
//printMenu loppuu ----------------------------------------------------------------
}

