/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tar09_btree;

/**
 *
 * @author kamaj
 */
class Node {
	int data;
	Node left;
	Node right;

	public Node(int data) {
		this.data = data;
		left = null;
		right = null;
	}
    
}

