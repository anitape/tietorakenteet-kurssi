package tar12_binaryheap;

public class BinaryHeapMain {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BinaryHeap heap = new BinaryHeap(15);
        int value;
        
        System.out.println("Adding values to Binary Heap...");
        for (int i=0; i<10; i++){
            value = (int) (Math.random()*100);
            heap.insert(value);
        }
        System.out.println("\nPrinting all added values.");
        heap.print();
        System.out.println("\nRemoving the minimum value from Binary Heap");
        heap.removeMin();
        heap.print();
        System.out.println("\nRemoving the minimum value from Binary Heap");
        heap.removeMin();
        heap.print();
        
        
    }

}
