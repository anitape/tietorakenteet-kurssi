package tar02_stackqueueproject;

public class Queue {
	private ListItem mHead;
	private ListItem mTail;
	private int mSize;
	
	// luo uusi lista-alkio ja kytke se jonon häntään (mTail)
	public void push(String aData) {
		ListItem newItem = new ListItem();
		newItem.setData(aData);
		
		if (mTail == null) { // onko tyhjä
			mTail = mHead = newItem;
		}
		
		else {
			mTail.setNext(newItem); // kytkentä...
			mTail = newItem; // uusi alkio uudeksi häntäalkioksi			
		}
		
		mSize++;		
	}
	
	// poistetaan alkio jonon päästä (mHead)
	// jos pinossa ei ole alkioita palautetaan null
	public ListItem pop() {
		ListItem returned = null;
		if (mHead != null) { // onko jonossa alkioita?
			returned = mHead;
			mHead = mHead.getNext();
		}
		if (mHead == null) // jos jono tyhjenee, huolehditaan, että kumpik
			mTail= null;
		return returned;
	}
	
	// listataan jonon sisältö
	public void printItems() {
		ListItem scanner = mHead;
		while (scanner != null) {
			System.out.print(scanner.getData()+",");
			scanner = scanner.getNext();
		}
	}
	
	// palautetaan alkioiden lukumäärä
	public int getSize() {
		return mSize;
	}
	
}
